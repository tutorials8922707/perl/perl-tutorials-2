#!/usr/bin/env perl

use strict;
use warnings;

sub func {
	my ($a) = @_;
	shift @$a;
	print @$a, "\n";
}

my @arr = (1, 2, 3, 4, 5);
my %hash = ("a", => "asfd", "b" => "asdkfjasdf", "c" => "asjkfd");

func(\@arr);
print "\n";
print @arr, "\n";

# my %h = ("1", "2", "3", "4");
# my @k = keys %h;
# my @v = values %h;
# print "@k\n@v\n";
