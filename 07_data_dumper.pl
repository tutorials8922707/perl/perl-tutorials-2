#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;

my $scalar = "string";

my @array = ("1", 2, 5, 1.0);

my %hash = (
	name => "nedDev",
	height => "not that tall",
	interest => ["football", "running", "boxing"],
);

print Dumper(\$scalar);
print Dumper(\@array);
print Dumper(\%hash);
