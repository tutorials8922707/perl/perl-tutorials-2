#!/usr/bin/env perl

use strict;
use warnings;

# Open file
open(my $fh, "<", "./info.log");

# Read file
# print <$fh>;

while (<$fh>) {
	print $_;
}

close($fh);

# Write to a file
# "<" read only
# ">" create a new file if it doesn't exists, or remove all contents if it does
# ">>" create a new file if it doesn't exists, append to end of file

open(my $fh2, ">", "./info.log2");

print $fh2 "NEW LOG FILE\n";

# Close the file handle
close($fh2);

# print $fh2 "NED DEV VID\n";
