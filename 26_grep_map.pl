#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;

my @newMap = map{$_ + 1} (1, 2, 3, 4, 5, 6, 7, 8);
my @newGrep = grep{$_ + 1} (1, 2, 3, 4, 5, 6, 7, 8);

print Dumper(\@newMap);
print Dumper(\@newGrep);

my @newMap2 = map{$_ % 2 == 0} (1, 2, 3, 4, 5, 6, 7, 8);
my @newGrep2 = grep{$_ % 2 == 0} (1, 2, 3, 4, 5, 6, 7, 8);

print Dumper(\@newMap2);
print Dumper(\@newGrep2);
