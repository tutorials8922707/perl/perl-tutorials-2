#!/usr/bin/env perl

use strict;
use warnings;

my @names = ("Ned", "Dev", "Sam");

for (@names) {
	print $_, "\n";
}

print "\n";

for (reverse @names) {
	print $_, "\n";
}

my $number = 12345;

my $numberReversed = reverse $number;
print $numberReversed, "\n";

my $string = "NedDev";

my $stringReversed = reverse $string;
print $stringReversed, "\n";
