#!/usr/bin/env perl

use strict;
use warnings;

# Assigning a name to a variable
my $name = "nedDev";

if (1 == 1 ) {
	# SCOPE B
	my $name = "Bernie";
	print $name, "\n";
}

# print $dogName, "\n";

print $name, "\n";

