#!/usr/bin/env perl

use strict;
use warnings;

# Basic IF
if (1) {
	print "Evaluates as true\n";
}
if (0) {
	print "Evaluates as false\n";
}

# IF ELSE
if (1) {
	print "Evaluates as true\n";
} else {
	print "Evaluates as false\n";
}
if (0) {
	print "Evaluates as true\n";
} else {
	print "Evaluates as false\n";
}

# IF ELSIF ELSE
if (0) {
	print "1. Evaluates as true\n";
} elsif (1) {
	print "2. Evaluates as true\n";
} else {
	print "Evaluates as false\n";
}

# UNLESS
unless (0) {
	print "UNLESS Evaluates as false\n";
}

print "Joe\n" if (1);
print "Joe\n" if (0);
print "Joe\n" unless (1);
print "Joe\n" unless (0);
