#!/usr/bin/env perl

use strict;
use warnings;

my $thing = "";

if (0) {
	$thing = "Ned";
} else {
	$thing = "Dev";
}

print $thing, "\n";

my $thing2 = 0 ? "Ned" : "Dev";
print $thing2, "\n";

# IF ELSEIF Ternary operator

my $thing3 = "";

if (1) {
	$thing3 = "Ned";
} elsif (1) {
	$thing3 = "water bottle";
} else {
	$thing3 = "Dev";
}

print $thing3, "\n";

my $thing4 = 1 ? "Ned" : 1 ? "water bottle" : "Dev";

print $thing4, "\n";
