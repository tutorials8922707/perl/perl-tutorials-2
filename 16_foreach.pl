#!/usr/bin/env perl

use strict;
use warnings;

my @arrayOfAnimals = ("Gerbils", "Hamsters", "Rats");

# explicit iterator
foreach my $animal (@arrayOfAnimals) {
	print $animal, "\n";
}

for my $animal (@arrayOfAnimals) {
	print $animal, "\n";
}

# implicit iterator
my @numbers = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
foreach (@numbers) {
	print $_, "\n";
}

