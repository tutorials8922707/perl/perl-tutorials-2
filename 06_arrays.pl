#!/usr/bin/env perl

use strict;
use warnings;

my @array = ("string1", "string2", "string3");

my @list_of_things = ("String", 1, 0.1, undef);

my $thing = "another string";
my @list_of_things2 = ("hello", 1, 5, $thing);

# Accessing Arrays
my $element = $array[1];

# DONT DO THIS
my $neat = "string";
my @neat = ("string", "string2", $element);
$neat[2];

# qw arrays
my @quotedWordArray = qw(each of these words is an array element);
my @quotedWordArraySlash = qw/each of these words is and array element/;

# Length of array
print scalar @quotedWordArray, "\n";
