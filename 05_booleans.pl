#!/usr/bin/env perl

use strict;
use warnings;

# FLASE: undef, 0, 000000000000000, '0', ''
# Everithing else is true

# my $statement = undef;
# my $statement = 0;
# my $statement = 000000000;
# my $statement = '0';
# my $statement = '';

# my $statement = 'asldfj';
# my $statement = 1;
# my $statement = '00000';
my $statement = 'false';

if ($statement) {
	print "True\n";
} else {
	print "False\n";
}
