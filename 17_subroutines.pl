#!/usr/bin/env perl

use strict;
use warnings;

# Structure of subroutines
sub routineName {
	# code that runs
	print "NedDev\n";
}

# routineName();

# old way
# &routineName();

sub makeUpperCase {
	my ($string, $string2) = @_;
	print uc $string, "\n";
}
# makeUpperCase("Ned");

sub makeUpperCase2 {
	my $string = shift @_;
	print uc $string, "\n";
}
# makeUpperCase2("Dev");

sub printHashValue {
	my (%hash) = @_;

	for my $key (keys %hash) {
		print $hash{$key}, "\n";
	}
}

my %hashOfNames = (
	one => "Ned",
	two => "Dev",
);

# printHashValue(%hashOfNames);

sub sumsArrayElements {
	my (@array) = @_;

	my $runningTotal = 0;
	for my $x (@array) {
		$runningTotal = $runningTotal + $x;
	}

	return $runningTotal;
}

my @arrayOfNums = (1, 2, 3, 4, 5);

print sumsArrayElements(@arrayOfNums), "\n";
