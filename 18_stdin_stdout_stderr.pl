#!/usr/bin/env perl

use strict;
use warnings;

# STDIN, STDOUT, STDERR

# my $input = <STDIN>;
# print $input;

# my $input = <>;
# print $input;

# STDOUT, STDERR

# my $name = "NedDev";
# print STDOUT $name;

# my $fName = "Ned\n";
# my $lName = "Dev\n";
#
# print STDOUT $fName;
# print STDERR $lName;

print STDOUT "What is your name?\n";
my $name = <STDIN>;

if ($name ne "") {
	print STDOUT "Your name is $name";
} else {
	print STDERR "Please enter a name...";
}
