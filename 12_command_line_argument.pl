#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;

# print Dumper(\@ARGV);
#
# my $firstElem = $ARGV[0];
# print $firstElem, "\n\n";
#
# my ($firstArrayElem, $secondArrayElem) = @ARGV;
# print $firstArrayElem, "\n", $secondArrayElem, "\n";

my ($word) = @ARGV;
print length($word), "\n";
