#!/usr/bin/env perl

use strict;
use warnings;

# Basic Scalar types
my $string = "str";
my $integer = 5;
my $float = 0.1;

print "$string\n";
print "$integer\n";
print "$float\n";

my $string2 = "0.5";


# Other Scalar types
my $undefined = undef;
# print "$undefined\n";

# Scalar reference
my $fName = "Ned";
my $lName = "Dev";

my $fullName = $fName . " " . $lName;
print "Full name is... $fullName\n";
