#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;

my %countries = (
	england => "small",
	australia => "large",
	germany => "medium",
);

# Accessing
my $sizeOfEngland = $countries{"england"};

# Add
$countries{"peru"} = "very large";

# Change
$countries{"england"} = "very very large";

# Delete
delete $countries{"peru"};

print Dumper(\%countries);

# qw hash
my %hash_food_wq = qw(
	burger 500
	chips 200
	cola 400
);
