#!/usr/bin/env perl

use strict;
use warnings;

# Count up from 1 to 10, in increments of 1
for (my $i = 0; $i <= 10; $i++) {
	print $i, "\n";
}

print "\n";

# Count up from 1 to 10, in increments of 1
for (my $i = 0; $i <= 10; $i = $i + 1) {
	print $i, "\n";
}

print "\n";

# Count down from 10 to 1, in increments of 1
for (my $x = 10; $x >= 1; $x--) {
	print $x, "\n";
}
